// JSON OBJECTS
// JSON stands for JavaScript Object Notation
// used for serializing different data types into bytes
// serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
// byte = ? binary digits (1 and 0) that is used to represent a character

/*
JSON DATA FORMAT
Syntax:
{
	"propertyA":"valueA",
	"propertyB":"valueB",

}

*/


/*
JS OBJECT
{
	city:"QC",
	province:"Metro Manila",
	country:'Philippines'

}


JSON
{
	"city":"QC",
	"province":"Metro Manila",
	"country":"Philippines"

}

JSON ARRAY
'cities': [
	{
		"city":"QC",
		"province":"Metro Manila",
		"country":"Philippines"
	}
	{
		"city":"QC",
		"province":"Metro Manila",
		"country":"Philippines"
	}
	{
		"city":"QC",
		"province":"Metro Manila",
		"country":"Philippines"
	}
	
]

*/

/*
JSON METHODS

the JSON contains methods for parsing and converting data into stringified JSON

Server language : stringify

client language : parse


Stingified JSON is a javascript object converted into string to be used in other function of JS applicaton

*/

let batchesArr = [
	{
		batchName: 'Batch X'
	},
	{
		batchName: 'Batch Y'
	}

]


console.log('Result from strigify method')
console.log(JSON.stringify(batchesArr))
// change object to JSON format



// we can also directly put the function inside the JSON function
let data = JSON.stringify({
	name:'John',
	age: 31,
	address: {
		city:'Manila',
		country: 'Philippines'
	}

}
	)

console.log(data)


// mini activity

/*

let firstName = prompt("Enter First Name: ")
let lastName = prompt("Enter Last Name: ")
let age = prompt("Enter Age: ")
let address = [prompt("Enter City: "), prompt("Enter Country: "), prompt("Enter Zip Code: ")]


let user = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(user)
*/

// end activity

// converting stringified JSON into JS objects
// information is commonly sent to applications in stringified JSON then converted back into objects
// this happens both for sending information to a backend app and sending information back to front end app
// upon receiving data JSON text can be converted to a JS Object with parse

let bacthesJSON = `[
	{
		"batchName": "Batch X"
	},
	{
		"batchName": "Batch Y"
	}
]`

console.log('result from parse method:')

console.log(JSON.parse(bacthesJSON))

let stringifiedObject = `
{
	"name":"John",
	"age":"31",
	"address":{
		"city":"Manila",
		"country":"Philippines"
	}

}`
console.log('result of parse')
console.log(JSON.parse(stringifiedObject))